```c

/*
  
Informations de Batterie et Wifi ainsi que GPS sur port série


Au cours de cette séance j'ai réalisé plusieurs tests du GPS, j'ai également réfélchis au fait de localiser la clé grâce à un système d'appareillage bluetooth,
de plus j'ai combiné les codes de Wifi, Batterie et GPS au sein du même programme. Ayant rélféchis l'idéal serait de scaner les AP autour de la clé, si la clé est 
transportée, l'utilisateur se connectera avec son appareil bluetooth afin d'avoir une authentification et on aura la localisation avec le GPS. Egalement le GPS pourrai
renvoyer ses infos de localisation sur un site ou bien sur la carte mémoire interne.

Le prochain étudiant devrai avoir à gérer la detection d'AP ainsi que l'éventualité de générer une page google Maps pour la localisation.
*/

#include <Wire.h>
#include "rgb_lcd.h"
#include <LBattery.h>
#include <LGPS.h>

rgb_lcd lcd;



#include <LWiFi.h>
#include <LWiFiClient.h>
#define WIFI_AP "LinkitOneWifi"
#define WIFI_PASSWORD ""
#define WIFI_AUTH LWIFI_OPEN  // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP.

gpsSentenceInfoStruct info;
char buff[256];

const int colorR = 255;
const int colorG = 255;
const int colorB = 255;
IPAddress ip ;

static unsigned char getComma(unsigned char num,const char *str)
{
  unsigned char i,j = 0;
  int len=strlen(str);
  for(i = 0;i < len;i ++)
  {
     if(str[i] == ',')
      j++;
     if(j == num)
      return i + 1; 
  }
  return 0; 
}

static double getDoubleNumber(const char *s)
{
  char buf[10];
  unsigned char i;
  double rev;
  
  i=getComma(1, s);
  i = i - 1;
  strncpy(buf, s, i);
  buf[i] = 0;
  rev=atof(buf);
  return rev; 
}

static double getIntNumber(const char *s)
{
  char buf[10];
  unsigned char i;
  double rev;
  
  i=getComma(1, s);
  i = i - 1;
  strncpy(buf, s, i);
  buf[i] = 0;
  rev=atoi(buf);
  return rev; 
}

void parseGPGGA(const char* GPGGAstr)
{
  /* Refer to http://www.gpsinformation.org/dale/nmea.htm#GGA
   * Sample data: $GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47
   * Where:
   *  GGA          Global Positioning System Fix Data
   *  123519       Fix taken at 12:35:19 UTC
   *  4807.038,N   Latitude 48 deg 07.038' N
   *  01131.000,E  Longitude 11 deg 31.000' E
   *  1            Fix quality: 0 = invalid
   *                            1 = GPS fix (SPS)
   *                            2 = DGPS fix
   *                            3 = PPS fix
   *                            4 = Real Time Kinematic
   *                            5 = Float RTK
   *                            6 = estimated (dead reckoning) (2.3 feature)
   *                            7 = Manual input mode
   *                            8 = Simulation mode
   *  08           Number of satellites being tracked
   *  0.9          Horizontal dilution of position
   *  545.4,M      Altitude, Meters, above mean sea level
   *  46.9,M       Height of geoid (mean sea level) above WGS84
   *                   ellipsoid
   *  (empty field) time in seconds since last DGPS update
   *  (empty field) DGPS station ID number
   *  *47          the checksum data, always begins with *
   */
  double latitude;
  double longitude;
  int tmp, hour, minute, second, num ;
  if(GPGGAstr[0] == '$')
  {
    tmp = getComma(1, GPGGAstr);
    hour     = (GPGGAstr[tmp + 0] - '0') * 10 + (GPGGAstr[tmp + 1] - '0');
    minute   = (GPGGAstr[tmp + 2] - '0') * 10 + (GPGGAstr[tmp + 3] - '0');
    second    = (GPGGAstr[tmp + 4] - '0') * 10 + (GPGGAstr[tmp + 5] - '0');
    
    sprintf(buff, "UTC timer %2d-%2d-%2d", hour, minute, second);
    Serial.println(buff);
    
    tmp = getComma(2, GPGGAstr);
    latitude = getDoubleNumber(&GPGGAstr[tmp]);
    tmp = getComma(4, GPGGAstr);
    longitude = getDoubleNumber(&GPGGAstr[tmp]);
    sprintf(buff, "latitude = %10.4f, longitude = %10.4f", latitude, longitude);
    Serial.println(buff); 
    
    tmp = getComma(7, GPGGAstr);
    num = getIntNumber(&GPGGAstr[tmp]);    
    sprintf(buff, "satellites number = %d", num);
    Serial.println(buff); 
  }
  else
  {
    Serial.println("Not get data"); 
  }
}





void setup() 
{
Serial.begin(115200);
  LGPS.powerOn();
  Serial.println("LGPS Power on, and waiting ..."); 
  delay(3000);
  //Batterie
  pinMode(13, OUTPUT);
    // set up the LCD's number of columns and rows:
    lcd.begin(16, 2);
    
    lcd.setRGB(colorR, colorG, colorB);
    
    // Print a message to the LCD.
    lcd.print ("Battery: ");
    lcd.print(LBattery.level());
    lcd.print ("%");
    
    
    lowBattery(); 
    delay(10);

//Wifi
 LWiFi.begin();
  //Serial.begin(9600);
  // keep retrying until connected to AP
  //Serial.println("Connecting to AP");
  while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD)))
  {
    delay(1000);
  }

  //on verifie si l'on est connecté
  if (LWiFi.RSSI()!=NULL){
    lcd.setCursor(5, 1);
    lcd.print ("W OK");
  }
// la non connexion n'est pas affichée!!!!
  else {
    lcd.setCursor(5, 1);
    lcd.print ("W NO");
  }
  
  printWifiStatus();
  
pinMode(2, OUTPUT);
    
}

void loop() 
{

  Serial.println("LGPS loop"); 
  LGPS.getData(&info);
  Serial.println((char*)info.GPGGA); 
  parseGPGGA((const char*)info.GPGGA);
  delay(2000);
    // set the cursor to column 0, line 1
    // (note: line 1 is the second row, since counting begins with 0):
    //lcd.setCursor(0, 1);

    //niveau de batterie critque active buzzer
     if (LBattery.level() <= 33){
    digitalWrite(2, HIGH);
  delay(100);
  digitalWrite(2, LOW);
  delay(100);
     }
    
}

void lowBattery(){
  int level;
  level = LBattery.level();
  if (LBattery.level() <= 33){
    
    lcd.setRGB(255, 0, 0);
 
  lcd.setCursor(0, 1);
    lcd.print ("B NO");
    digitalWrite(13,HIGH);
        delay(750);
        digitalWrite(13,LOW);
        delay(750);

  }
  else {
      lcd.setCursor(0, 1);
      lcd.print("B OK");
  }

 }



 void printWifiStatus()
{
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(LWiFi.SSID());
  // print your WiFi shield's IP address:
  ip= LWiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  Serial.print("subnet mask: ");
  Serial.println(LWiFi.subnetMask());
  Serial.print("gateway IP: ");
  Serial.println(LWiFi.gatewayIP());
  // print the received signal strength:
  long rssi = LWiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");

}




/*********************************************************************************************************
  END FILE
*********************************************************************************************************/

```